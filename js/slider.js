
function slider() {
    if($(window).width()<767) {
        var step = $(window).width();
    }else {
        var step = $(window).width() / 3;
    }

    $('.slider .wr > div').css({width: step})
    let count = $('.slider .wr > div').length
    $('.slider .wr').width(count * (step))

    if($(window).width()<767) {
        var curent = 1;
    }else{
        var curent = 2;
    }
    let pos = 0;
    $(`.slider .wr div:nth-child(${curent}) img`).css({opacity: 1})

    let dot = ''
    for (let i = 1; i <= count; i++) {
        dot += `<a href="" data-num="${i}"></a>`
    }
    $('.dot').html(dot)


    $('.slider .left').on('click', toleft)
    $('.slider .right').on('click', toright)


    function toleft() {

        if(pos<0) {

            if ($(window).width() > 767) {
                $(`.slider .wr div img`).css({opacity: 0.2})
            }
            pos += step
            curent--
            $('.slider .wr').animate({marginLeft: pos}, 300, function () {
                $(`.slider .wr div:nth-child(${curent}) img`).css({opacity: 1})
                $(`.dot a`).removeClass('active')
                $(`.dot a[data-num='${curent}']`).addClass('active')

            })
        }
    }

    function toright() {

        if(curent<=count) {
            console.log(curent, count)
            if ($(window).width() > 767) {
                $(`.slider .wr div img`).css({opacity: 0.2})
            }
            pos -= step
            curent++
            $('.slider .wr').animate({marginLeft: pos}, 300, function () {
                $(`.slider .wr div:nth-child(${curent}) img`).css({opacity: 1})
                $(`.dot a`).removeClass('active')
                $(`.dot a[data-num='${curent}']`).addClass('active')

            })
        }
    }

}

$(window).resize(slider)

slider()