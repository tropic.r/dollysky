let ob = document.querySelector('.popup-item #run-ordering')
let pp = document.querySelector('.over')
let oc = document.querySelector('#order-confirm')

let commit = document.querySelector('.over textarea[name=commit]')
let policy = document.querySelector('.over input[name=policy]')
let clothes = document.querySelector('.over input[name=clothes]')
let width = document.querySelector('.over input[name=width]')
let height = document.querySelector('.over input[name=height]')
let dpi = document.querySelector('.over select[name=dpi]')
let unit = document.querySelector('.over select[name=unit]')
let email = document.querySelector('.over input[name=email]')
let img = document.querySelector('.over input[name=files]')
let cartError = document.querySelector('.over .cart-error')
let boxfile = document.querySelector('.over #cart-select-file')
let sum = document.querySelector('.over .sum span')
let fullPriceBox = document.querySelector('.over .bottom-sum h3 span')
let gbSelect = document.querySelector('.popup-item .select select')
let hiddens = document.querySelectorAll('.hiddens')

let createOrderLink = 'https://dsc.hopto.org/dj/crm/apis/v1/orderlist/'
let createCardLink = 'https://dsc.hopto.org/dj/crm/apis/v1/ordercardlist/'
let sendFilesLink = 'https://dsc.hopto.org/dj/crm/apis/v1/orderimagelist/'
var redirect_link = 'https://dsc.hopto.org/crm/'
let category_link = 'https://dsc.hopto.org/dj/crm/apis/v1/category_of_products/'
ob.addEventListener('click', orderStart)

//dpi.addEventListener('change', getUnit)

let iprice = 0, price_for_retoucher = 0, title = '', category = 0,
    id = 0, background = false, backgroundId = 0, countFile = 0, fullPrice = 0,
    currency = 0, unitid = 0, description = '', rush = false, needfiles = false
let eaccess = true, haccess = true, waccess = true, faccess = true, raccess = true
var idcard = [], filesArray = {};


function controlFields(idcat) {
    let response = xhr(false, category_link)
    response.done((obj) => {
        obj.forEach((cat) => {
            if (idcat == cat.id) {
                needfiles = cat.need_files
            }
        })
        if (needfiles) {
            //boxfile.style = 'display:block'
            hiddens.forEach((elem) => {
                elem.style = 'display:block'
            })
        } else {
            // boxfile.style = 'display:none'
            hiddens.forEach((elem) => {
                elem.style = 'display:none'
            })
        }
    })
}


function orderStart() {
    document.body.style = 'overflow:hidden'
    pp.style.display = 'block'
    id = this.getAttribute('data-id')
    let itemJson = getItemById(id)

    getUnit()

    description = itemJson.description

    background = gbSelect.options[gbSelect.selectedIndex].innerHTML

    backgroundId = $('.t-descr_xxs').val()


    itemJson.done(function (response) {
        currency = response.currency
        rush = itemJson.rush
        controlFields(response.category)
        iprice = response.price
        price_for_retoucher = response.price_for_retoucher
        title = response.title
        category = response.category
        rush = response.rush

        let opt = gbSelect.options[gbSelect.selectedIndex].innerHTML

        $('.over .sum span').html(iprice)
        $('.over .bottom-sum h3 span').html(iprice)
        $('.over .cart-title').html(`<span>${title}</span><span style="font-size: 13px">Фон: ${opt}</span>`)


        let imgJson = getPictureById(id);

        imgJson.done(function (responseImg) {

            responseImg.forEach((objImg) => {

                if (objImg.title == background) {
                    $('.over .cart-img').html(`<img src="${objImg.image}">`)
                }

            })

        })


    })
}


function xhr(obj = false, url) {
    if (obj == false) {
        return $.ajax({
            url: url,
            type: 'get',
            complete: function (back) {
                //console.log(back)
            },
            error: function () {
                console.error('xhr error')
            }
        })
    } else {
        return $.ajax({
            url: url,
            type: 'post',
            data: obj,
            complete: function (back) {
                //console.log(back)
            },
            error: function () {
                console.error('xhr error')
            }
        })
    }
}


function validateFields() {


    return new Promise((resolve, reject) => {

        if (email.value.match(/^[a-zA-Z0-9\_\.]{2,35}@{1}\w{1,35}\.{1}\w{2,6}\.{0,1}\w{0,6}$/g) != null) {
            valid('.over input[name=email]')
            eaccess = true
        } else {
            eaccess = false
            notvalid('.over input[name=email]')
        }

        if (width.value.match(/^[0-9]{1,35}$/g) != null) {
            valid('.over input[name=width]')
            waccess = true
        } else {
            waccess = false
            notvalid('.over input[name=width]')
        }

        if (height.value.match(/^[0-9]{1,35}$/g) != null) {
            valid('.over input[name=height]')
            haccess = true
        } else {
            haccess = false
            notvalid('.over input[name=height]')
        }

        if (policy.checked) {
            valid('.over #rules-box')
            raccess = true
        } else {
            raccess = false
            notvalid('.over #rules-box')
        }

        if (Object.keys(filesArray).length > 0 && Object.keys(filesArray).length <= 10) {
            valid('.over #cart-select-file')
            faccess = true
        } else {
            faccess = false
            notvalid('.over #cart-select-file')
        }

        resolve();
    })

}


function orderCreate() {

    return new Promise((resolve, reject) => {
        oc.innerHTML='<img src="img/load-mini.gif" style="height: 40px">'
        oc.setAttribute('disabled','disabled')
        //unitStr = dpi.options[dpi.selectedIndex].innerHTML

        let order = {}
        order.amount = fullPrice
        order.amount_for_retoucher = price_for_retoucher * img.files.length
        order.category = category
        order.comment = commit.value
        order.email = email.value
        order.product = id
        order.total_images = countFile
        order.order_status = 1
        order.currency = currency
        order.unit = +unit.value
        order.resolution = +dpi.value
        order.height = +height.value
        order.width = +width.value
        order.background = +gbSelect.value
        fullPriceBox.innerHTML = iprice * img.files.length


        let createOrderResponseApi = xhr(order, createOrderLink)

        createOrderResponseApi.done(function (x) {
            idcard = []

            for (let i = 0; i < countFile; i++) {
                let card = {}
                card.amount = iprice
                card.amount_for_retoucher = x.amount_for_retoucher
                card.unconfirmed = 1
                card.image_status = 1
                card.height = +height.value
                card.width = +width.value
                card.rush = rush
                card.order = x.id
                card.product = x.product
                card.background = backgroundId
                card.resolution = +dpi.value
                card.change_dress = clothes.checked
                order.unit = +unit.value
                card.currency = currency

                let cardResponseApi = xhr(card, createCardLink)
                cardResponseApi.done(function (idd) {
                    idcard[i] = idd.id
                })

            }
            resolve();

        })
    })

}


function crearField() {
    filesArray={}
    idcard=0
    countFile=0
    commit.value=""
    policy.checked = false
    clothes.checked = false
    width.value=''
    height.value=''
    email.value=''
    img.value=''
    $('#filelist').html('')
    $('#trigger-file').html('Загрузить файлы')
}


function sendFile(cnt) {

    return new Promise((resolve, reject) => {

        setTimeout(function () {


            if(cnt<countFile){

                var file_data = filesArray[cnt];

                var form_data = new FormData();
                form_data.append('orig_image', file_data);
                form_data.append('order_card', idcard[cnt]);
                form_data.append('main', true);
                form_data.append('title', file_data.name);

                let resp = $.ajax({
                    url: sendFilesLink,
                    dataType: 'text',
                    async:true,
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (file_response) {
                        if(cnt+1<countFile){
                            oc.innerHTML=`<img src="img/load-mini.gif" style="height: 40px"> отправлено ${cnt+1} из ${countFile}`
                            cnt+=1
                            sendFile(cnt)
                        }
                        else if(cnt+1==countFile){
                            oc.innerHTML=`Успешно отправлено ${cnt+1} из ${countFile}`
                            oc.removeAttribute('disabled')
                            crearField()
                            setTimeout(function () {
                                window.location.href = redirect_link
                            },1000)
                            resolve()
                            return
                        }


                    },
                    complete: function (back) {
                        //console.log(back)
                    },
                    error: function () {
                        console.error('files sending error')
                        oc.innerHTML='Оформить заказ'
                        oc.removeAttribute('disabled')
                        sendFile(cnt)
                    }
                });

            }


        }, 500)


    })

}


function valid(elem) {
    document.querySelector(elem).classList.remove('notvalid')
}


function notvalid(elem) {
    document.querySelector(elem).classList.add('notvalid')
}


function fileList(del) {

    if (del == false) {
        if (Object.keys(filesArray).length == 0) {

            for (var i = 0; i < img.files.length; i++) {

                filesArray[i] = img.files.item(i)

            }

        } else {
            $.each(img.files, function (k, v) {
                if (isset(v.name) == false) {
                    filesArray[Object.keys(filesArray).length] = v
                }
            })
        }

    }else{
        let tmp={}
        let c = 0
        $.each(filesArray, function (k, v) {
            tmp[c] = v
            c++
        })
        filesArray = tmp

    }

    function isset(name) {
        let iss = false
        $.each(filesArray, function (key, val) {
            if (name == val.name) {
                iss = true
            }
        })
        return iss;

    }

    let list = ''
    console.log(filesArray)
    if (Object.keys(filesArray).length <= 10) {
        countFile = Object.keys(filesArray).length
        fullPrice = iprice * countFile
        fullPriceBox.innerHTML = fullPrice
        sum.innerHTML = fullPrice


        $('#trigger-file').html('Выбрано ' + Object.keys(filesArray).length + ' файл(ов)')

        $.each(filesArray, function (k, v) {
            list += `<span>${v.name} (${v.type}) <i class="fas fa-trash rem-file" data-id="${k}"></i></span>`
        })

        $('#filelist').html('<b>Выбранные файлы:</b> <br><br>' + list)

        valid('.over #cart-select-file')
    } else if (Object.keys(filesArray).length == 0) {
        notvalid('.over #cart-select-file')
        $('#filelist').html('<span style="color:red">Файлы изображений не загружены</span>')
    } else if (Object.keys(filesArray).length > 10){
        countFile = Object.keys(filesArray).length
        fullPrice = iprice * countFile
        fullPriceBox.innerHTML = fullPrice
        sum.innerHTML = fullPrice


        $('#trigger-file').html('Выбрано ' + Object.keys(filesArray).length + ' файл(ов)')

        $.each(filesArray, function (k, v) {
            list += `<span>${v.name} (${v.type}) <i class="fas fa-trash rem-file" data-id="${k}"></i></span>`
        })

        $('#filelist').html('<span style="color:red">Максимально допустимое количество файлов 10</span><br><b>Выбранные файлы:</b> <br><br>' + list)

        notvalid('.over #cart-select-file')
        // $('#filelist').html('<span style="color:red">Максимально допустимое количество файлов 10</span>')
    }
    removeFile()

}

function removeFile() {
    let table = document.querySelectorAll('.rem-file')
    table.forEach((elem) => {
        elem.addEventListener('click', function () {
            let id = this.getAttribute('data-id')
            delete filesArray[id];
            fileList(true)
        })
    })
}


$('input[name=files]').change(function () {
    fileList(false)
})


$('#trigger-file').on('click', function () {
    $('input[name=files]').trigger('click')
})

oc.addEventListener('click', run)


unit.addEventListener('change', function () {
    let unitstr = this.options[this.selectedIndex].innerHTML
    width.setAttribute('placeholder', unitstr)
    height.setAttribute('placeholder', unitstr)
})

function getUnit() {
    let r = xhr(false, "https://dsc.hopto.org/dj/crm/apis/v1/units/")

    let unitStr = ''
    r.done(function (h) {
        h.forEach((obj) => {
            unitStr += `<option value="${obj.id}">${obj.title}</option>`
        })
        $('select[name=unit]').html(unitStr)

    })
}


async function run() {
    await validateFields()

    if (needfiles) {
        if (eaccess && waccess && haccess && raccess && faccess) {
            cartError.style.height = '0px'
            cartError.innerHTML = ''

            await orderCreate()
            await sendFile(0)

        } else {
            cartError.style.height = '100px'
            cartError.innerHTML = '<h3>Пожалуйста, заполните поля правильно</h3>'
        }
    } else {
        if (eaccess && raccess) {
            cartError.style.height = '0px'
            cartError.innerHTML = ''

            await orderCreate()
            await sendFile()

        } else {
            cartError.style.height = '100px'
            cartError.innerHTML = '<h3>Пожалуйста, заполните поля правильно</h3>'
        }
    }

}


function cartClose() {
    pp.style = 'display:none'
}

$('#cart-close').on('click', cartClose)

$(document).mouseup(function (e) {
    var cart = $(".over .cart");
    if (cart.has(e.target).length === 0) {
        cartClose()
    }
});