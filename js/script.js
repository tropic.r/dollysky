"use strict"

function headerSize() {
    let screenY = window.innerHeight;
    let header = document.querySelector('.header')
    if(header) {
        header.style.height = `${screenY}px`
    }
}

headerSize();

window.addEventListener('resize',headerSize)


let soc_btn = document.querySelector('.soc-panel-ico')
let soc_panel = document.querySelector('.soc-panel')
let opened=false;

soc_btn.addEventListener('click', function () {
    if(!opened) {
        soc_panel.style = 'visibility:visible; opacity:1'
        opened=true
    }else if(opened) {
        soc_panel.style = 'visibility:hidden; opacity:0'
        opened=false
    }
})

let burger = document.querySelector('.burger-menu')
let menu = document.querySelector('.phone-menu')
let closeBtn = document.querySelector('.phone-menu-close')

burger.addEventListener('click', function () {
    menu.style = 'left:0px;'
})
closeBtn.addEventListener('click', function () {
    menu.style = 'left:-100%;'
})



let priceOpened = false;
let typeOpened = false;
let themeOpened = false;

$('#filter-price-btn').on('click', price)
$('#filter-type-btn').on('click', type)
$('#filter-theme-btn').on('click', theme)


$(document).on('click', function (e) {
    if($('.filter-price').has(e.target).length==0) {
        $('.filter-price').hide()
        priceOpened=false
    }

    if($('.filter-theme').has(e.target).length==0) {
        $('.filter-theme').hide()
        themeOpened=false
    }

    if($('.filter-type').has(e.target).length==0) {
        $('.filter-type').hide()
        typeOpened=false
    }
})


function price(e) {
    e.preventDefault();
    if(priceOpened==false) {
        let coord = $(this).offset();
        $('.filter-price').css({left: coord.left, top: coord.top + 40,display:'flex'})
        priceOpened=true

        $('.filter-theme').hide()
        themeOpened=false

        $('.filter-type').hide()
        typeOpened=false
    }else if(priceOpened){
        $('.filter-price').hide()
        priceOpened=false
    }
    return false;
}

function theme(e) {
    e.preventDefault();
    if(themeOpened==false) {
        let coord = $(this).offset();
        $('.filter-theme').css({left: coord.left, top: coord.top + 40,display:'flex'})
        themeOpened=true

        $('.filter-type').hide()
        typeOpened=false

        $('.filter-price').hide()
        priceOpened=false
    }else if(themeOpened){
        $('.filter-theme').hide()
        themeOpened=false
    }
    return false;
}


function type(e) {
    e.preventDefault();
    if(typeOpened==false) {
        let coord = $(this).offset();
        $('.filter-type').css({left: coord.left, top: coord.top + 40,display:'flex'})
        typeOpened=true

        $('.filter-price').hide()
        priceOpened=false

        $('.filter-theme').hide()
        themeOpened=false
    }else if(typeOpened){
        $('.filter-type').hide()
        typeOpened=false
    }
    return false;
}
