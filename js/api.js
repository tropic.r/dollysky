"use strict"



let query_string ='';
let query_array=[`page=1`];
let story_array=[];
let sort=`&ordering=id`


function check(){
    let q = window.localStorage.getItem('query')
    if(q !=null){
        let tmp = q.split('&')
        if(tmp.length>1){
            tmp.forEach(function (v) {
                if(v.match(/clipart_type/g) !=null){
                    document.querySelector(`input[data-value='${v}']`).checked=true
                }
                if(v.match(/clipart_decor/g) !=null){
                    document.querySelector(`input[data-value='${v}']`).checked=true
                }
                if(v.match(/price_min/g) !=null){
                    let n = v.split('=')
                    $('.price-from').val(n[1])
                }
                if(v.match(/price_max/g) !=null){
                    let n = v.split('=')
                    $('.price-to').val(n[1])
                }

            })
        }
    }


}
check()

$('#sort').on('change', function () {

    query_array=[`page=1`]
    story_array=[]
    sorting()
    getItems(false)
})

function sorting() {
    let srt = $('#sort').val()

    if(srt != 'false') {
        sort = `&ordering=${srt}`
    }else{
        sort = `&ordering=id`
    }
    console.log(sort)
}

$('.filter-price button').on('click',  function () {
    query_array=[`page=1`]
    story_array=[]
    filter()
})

$('.filter-type input, .filter-theme input').on('change',function () {
    query_array=[`page=1`]
    story_array=[]
    filter()
})

$('.product-top a').click(function (e) {
    e.preventDefault();
    query_array=[`page=1`]
    story_array=[]
    $('.product-top a').removeClass('active')
    $(this).addClass('active')
    filter();
})

function filter(append=false) {

    let pricefrom = $('.filter-price .price-from').val()
    let priceto = $('.filter-price .price-to').val()

    if(pricefrom.length>0 && priceto.length>0) {
        query_array.push(`price_min=${pricefrom}&price_max=${priceto}`)
    }

    let cat = $('.product-top .active').attr('data-cat');

    if(cat !=undefined){
        if(cat!='all'){
            query_array.push(cat)
        }
    }

    $('.filter-type input').each(function () {
        if($(this).is(':checked')){
            let val = $(this).attr('data-value')
            let title = $(this).attr('data-title')

            story_array.push(title+'|'+val)
            query_array.push(val)
        }
    })

    $('.filter-theme input').each(function () {
        if($(this).is(':checked')){
            let val = $(this).attr('data-value')
            let title = $(this).attr('data-title')

            story_array.push(title+'|'+val)
            query_array.push(val)
        }
    })

    query_string = query_array.join('&')
    window.localStorage.setItem('query',query_string)
    console.log(query_string)

    getItems(append)

    let s = ''

    story_array.forEach(function (v) {
        let h = v.split('|')
        s += ` <a class="lable" href="" data-target="${h[1]}"><i class="fas fa-times"></i> ${h[0]}</a>`
    })
    if(story_array.length>1) {
        s += '<a class="clr" href="#">Очистить все</a>'
    }
    window.localStorage.setItem('story',s)
    stories()
}

stories()

function stories() {
    $('.curent-filter').html(window.localStorage.getItem('story'))
}

getItems()

function getItems(append=true) {
    let pr='';

    $.ajax({

        url: 'https://dsc.hopto.org/dj/crm/apis/v1/products/?' + window.localStorage.getItem('query')+sort,
        success: function (data) {

            if (data.results != null) {
                $('.search-result').html(`Найдено: ${data.count}`)
                data.results.forEach((obj) => {

                    pr += `
                      <div class="item temp" data-id="${obj.id}">
                        <div style="background: whitesmoke;"><img src="img/noimage.jpg" alt="${obj.title}"></div>
                        <div><span>${obj.title}</span></div>
                        <div>${obj.price}р</div>
                        <div>
                            <button data-id="${obj.id}">Заказать</button>
                        </div>
                       </div>`;


                })
                if(append) {
                    $('.product-content').append(pr)
                }else{
                    $('.product-content').html(pr)
                }

                itemsPopup();
                setImages();
                $('.product-bottom button').css({'background': '#000'}).html('Загрузить еще')
            }


        },
        error: function () {
            console.error('api error');
        }
    })
}


function setImages() {
    $('.temp').each(function (i) {
        let _this = $(this)
        let id = _this.attr('data-id')


        let response = getPictureById(id)

        response.done(function (e) {
            if (e[0] != undefined) {

                _this.find('img').attr('src', e[0].image)

            }
            _this.delay(i * 200).animate({'opacity': '1'}, 500)
            _this.removeClass('temp')
        })

    })
}


function getPictureById(id) {
    let response = $.ajax({
        url: 'https://dsc.hopto.org/dj/crm/apis/v1/retouch_background/' + id
    })
    return response;
}


function getItemById(id) {
    let response = $.ajax({
        url: 'https://dsc.hopto.org/dj/crm/apis/v1/product/' + id
    })
    return response
}


function getResolutionList() {
    return $.ajax({
        url:'https://dsc.hopto.org/dj/crm/apis/v1/dpi/',
        error:function () {
            console.error('get resolutions is fail')
        }
    })
}


function itemsPopup() {
    let popupItem = document.querySelector('.popup-item')
    let orderBtn = document.querySelectorAll('.item button')
    let moreBtn = document.querySelector('.more')
    let popupItemClose = document.querySelector('.popup-item-close')

    $('body').on('click', '.item', popupWindow)

    orderBtn.forEach((btn) => {
        btn.addEventListener('click', popupWindow)
    })

    function popupWindow() {
        let id = this.getAttribute('data-id')
        popupItem.style.transform = 'scale(1)'
        document.body.style='overflow:hidden'


        let response = getItemById(id)
        let responseImg = getPictureById(id)
        let responseResolutions = getResolutionList()

        response.done(function (obj) {
            let popuptitle = document.querySelector('.popup-title');
            let popupprice = document.querySelector('.popup-price');
            let desc = document.querySelector('.popup-item .desc');
            let fullimg = document.querySelector('.popup-item .fullimg');
            let mini = document.querySelector('.popup-item .mini');
            let pb = document.querySelector('.popup-item #run-ordering')

            pb.setAttribute('data-id',id)
            popuptitle.innerHTML = obj.title
            popupprice.innerHTML = obj.price + 'р'
            desc.innerHTML = obj.text

            responseResolutions.done(function (res) {
                let resList = ''
                res.forEach((val)=>{
                    resList +=`<option value="${val.id}">${val.resolution}</option>`
                })
                document.querySelector('select[name=dpi]').innerHTML = resList
            })



            responseImg.done(function (img) {
                let miniP = '';
                let option ='';

                fullimg.innerHTML = `<img src="${img[0].image}" alt="${img[0].title}" title="${img[0].title}">`
                if (img.length > 1) {
                    img.forEach(function (val) {
                        miniP += `<img src="${val.image}" alt="${val.title}" title="${val.title}">`
                        option += `<option value="${val.id}">${val.title}</option>`
                    })
                    mini.innerHTML = miniP
                    $('.popup-item .select select').html(option)

                } else {
                    mini.innerHTML = ""
                }

                $('body').on('click', '.popup-item .mini img', function () {
                    let src = $(this).attr('src')

                    $('.fullimg img').attr('src', src)
                })


            })

        })
    }

    moreBtn.addEventListener('click', (e) => {
        e.preventDefault();
        popupItem.style.transform = 'scale(0)'
        document.body.style='overflow:auto'
    })
    popupItemClose.addEventListener('click', (e) => {
        e.preventDefault();
        popupItem.style.transform = 'scale(0)'
        document.body.style='overflow:auto'
    })
}


$('body').on('change','.popup-item .select select', function () {

    let opt = this.options[this.selectedIndex].innerHTML



    let clone = $(`img[title='${opt}']`).clone()
    $('.popup-item .fullimg').html(clone)
})


$('.product-bottom button').on('click', function () {
    $(this).html('<img src="img/load-mini.gif" style="height: 35px">').css({'background': 'gray'})
    let page = +$(this).attr('data-response')
    page++
    $(this).attr('data-response',page)
    story_array=[]
    query_array=[`page=${page}`]

    setTimeout(function () {
        filter(true);

    }, 1000)
})


$('body').on('click','.clr', function () {
    window.localStorage.setItem('query','page=1')
    window.localStorage.setItem('story','')
    $('.search-result,.curent-filter').html('')

    // $('.filter-type input:checked').uncheck()
    let btn1 = document.querySelector('.filter-type input:checked')
    if(btn1) {
        btn1.checked = false
    }
    let btn2 = document.querySelector('.filter-theme input:checked')
    if(btn2) {
        btn2.checked = false
    }
    getItems(false)

    return false
})


$('body').on('click','.lable', function (e) {
    e.preventDefault();
    let dataValue = $(this).attr('data-target')
    let t = document.querySelector(`input[data-value='${dataValue}']`)
    t.checked=false
    query_array=[`page=1`]
    story_array=[]
    filter();
})
